<!DOCTYPE html>
<html lang="en">
<head>
  <!-- Theme Made By www.w3schools.com - No Copyright -->
  <title>HEADER</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
   <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" /> 
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet" type="text/css">
 <link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.2/css/bootstrap.min.css" integrity="sha384-y3tfxAZXuh4HwSYylfB+J125MxIs6mR5FOHamPBG064zB+AFeWH94NdvaCBm8qnd" crossorigin="anonymous">
<link href="jquery.paginate.css" rel="stylesheet" type="text/css">
  <style>
 
  body{
	  margin-top:60px;
  }
  
  
  </style>
  </head>
  <?php include('header.php')?>
  <body style="margin-top:60px;">
</br>
<div class="container1">
	<div class="row"> 
	<div class="col-sm-2"></div>
	<div class="col-sm-8 ">
	<div class="form-group" style="width:280px; margin:15px;float:left; left:100px;">
			<label for="title">Select Bank:</label>
                <select name="BANK" class="form-control" id="bank" >
				<option value="">--- Select BANK ---</option>
				 <?php

                        require('newdbconfig.php');

                        $sql = "SELECT * FROM bank"; 

                        $result = $mysqli->query($sql);

                        while($row = $result->fetch_assoc()){

                            echo "<option value='".$row['BANK_ID']."'>".$row['BANK']."</option>";

                        }

                    ?>
                </select>
				</div>
 <div class="form-group" style="width:280px; margin:15px;float:left;">
                <label for="title">Select State:</label>
                <select name="STATE" class="form-control" id="state" >
                 </select>
            </div>
            <div class="form-group"style="width:280px; margin:15px; float:left;">
                <label for="title">Select District:</label>
                <select name="DISTRICT" class="form-control" id="dist"></select>
                                                                 </div>           						
            <div class="form-group" style="width:280px; margin:15px ;float:left;">
                <label for="title">Select city:</label>
                <select name="CITY" class="form-control" id="city"></select>
                                                                 </div>			
            <div class="form-group" style="width:280px; margin:15px;float:left;">
                <label for="title">Select Branch:</label>
                <select name="BRANCH" class="form-control"  id="brnch" onchange="fetch()"></select>
            </div>	
	</div>
	<div class="col-sm-2"></div>
	</div>
</div>
</br>
<div class="container2" style=" margin-left:30px;margin-right:30px;border-radius:8px;" >
	<div class="row"> 
	<div class="col-sm-2"></div>
	<div class="col-sm-8 " >
	  <input type="text" class="form-control" placeholder="Text input" name="searchele" id="searchele" required style="float:left;width:500px;height:47px;margin-left:171px;display:none;margin-top:39px;"/>
		<button type="submit" class="btn btn-danger" name="search" style="float:left;height:47px;margin-left:7px;display:none;margin-top:39px;">Search</button>
     <p id="result" style="position:relative;margin-left:-70px;margin-top:150px; width:100%;max-width:700px;min-width:700px;margin-right:60px;"> </p>
	</div>
	<div class="col-sm-2"></div>
	</div>
</div>
</br>
<hr>
<div class="container3 collapse navbar-collapse ">
	<div class="row"> 
	<div class="col-sm-2"></div>
	<div class="col-sm-8">
      <ul class="nav navbar-nav ">
	    <span></span>
		<li><a href="http://www.google.com">L1</a></li>
        <li><a href="#">L2</a></li>
		<li><a href="#">L3</a></li>
		<li><a href="#">L3</a></li>
      </ul>
	  </br>
	</br>
<h5>Disclamier:We have tried our best to keep the latest information updated as available from RBI, users are requested to confirm information with the respective bank before using the information provided. The author reserves the right not to be responsible for the topicality, correctness, completeness or quality of the information provided. Liability claims regarding damage caused by the use of any information provided, including any kind of information which is incomplete or incorrect, will therefore be rejected.</h5>
	</div>
	<div class="col-sm-2"></div>
	</div>
</div>
<script src="jquery.paginate.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script>
function fetch() {
	var st=document.getElementById("state").value;
	var dist=document.getElementById("dist").value;
	var city=document.getElementById("city").value;
	var brnch=document.getElementById("brnch").value;
	var bank=document.getElementById("bank").value;
    if (st == "" || dist=="" || city=="" || brnch==""||bank=="") {
        alert("ensure all select boxes are selected");
        return;
    } else {
        if (window.XMLHttpRequest) {
            
            xmlhttp = new XMLHttpRequest();
        } else {
            
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById("result").innerHTML = this.responseText;
					$("table").paginate({
           "elemsPerPage": 10,
            "maxButtons": 6
    });
	    $(".container2").css('background-color','#EEEEEE');
            }
        };
        xmlhttp.open("GET","fetch.php?state="+st+"&dist="+dist+"&city="+city+"&brnch="+brnch+"&bank="+bank,true);
        xmlhttp.send();
    }
}
function fetchbank(){
	var bank=document.getElementById("bank").value;
        if (window.XMLHttpRequest) {
            
            xmlhttp = new XMLHttpRequest();
        } else {
            
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById("result").innerHTML = this.responseText;
				$("table").paginate({
           "elemsPerPage": 10,
            "maxButtons": 6
    });
	
	$(".container2").css('background-color','#EEEEEE');
	
	     $('.btn.btn-danger,.form-control').css('display','inline-block');
	
            }
        };
        xmlhttp.open("GET","fetchbank.php?bank="+bank,true);
        xmlhttp.send();
    }
function fetchstate(){
	var bank=document.getElementById("bank").value;
	var st=document.getElementById("state").value;
        if (window.XMLHttpRequest) {
            
            xmlhttp = new XMLHttpRequest();
        } else {
            
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById("result").innerHTML = this.responseText;
					$("table").paginate({
           "elemsPerPage": 10,
            "maxButtons": 6
    });
	
	$(".container2").css('background-color','#EEEEEE;');
            }
        };
        xmlhttp.open("GET","fetchstate.php?bank="+bank+"&state="+st,true);
        xmlhttp.send();
}
function fetchdist(){
	var bank=document.getElementById("bank").value;
	var st=document.getElementById("state").value;
	var dist=document.getElementById("dist").value;
	if (window.XMLHttpRequest) {
            
            xmlhttp = new XMLHttpRequest();
        } else {
            
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById("result").innerHTML = this.responseText;
					$("table").paginate({
           "elemsPerPage": 10,
            "maxButtons": 6
    });
	$(".container2").css('background-color','#EEEEEE;');
            }
        };
        xmlhttp.open("GET","fetchdist.php?bank="+bank+"&state="+st+"&dist="+dist,true);
        xmlhttp.send();
   
}
function fetchcity(){
	
	var st=document.getElementById("state").value;
	var dist=document.getElementById("dist").value;
	var city=document.getElementById("city").value;
	var brnch=document.getElementById("brnch").value;
	var bank=document.getElementById("bank").value;

        if (window.XMLHttpRequest) {
            
            xmlhttp = new XMLHttpRequest();
        } else {
            
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById("result").innerHTML = this.responseText;
					$("table").paginate({
           "elemsPerPage": 10,
            "maxButtons": 6
    });
	$(".container2").css('background-color','#EEEEEE;');
            }
        };
        xmlhttp.open("GET","fetchcity.php?bank="+bank+"&state="+st+"&city="+city+"&dist="+dist,true);
        xmlhttp.send();
}

$( "select[name='BANK']" ).change(function () {
	var BANK_ID = $(this).val();
	fetchbank();
		
      if(BANK_ID){
        $.ajax({
            url: "ajax1.php",
            dataType: 'Json',
            data: {'id':BANK_ID},
            success: function(data) {
                $('select[name="STATE"]').empty();
				$('select[name="STATE"]').append('<option value="">--select state--</option>');
                $.each(data, function(key, value) {
                    $('select[name="STATE"]').append('<option value="'+ key +'">'+ value +'</option>');
                });
            }
        });
    }else{
        $('select[name="STATE"]').empty();
    }
});
$( "select[name='STATE']" ).change(function () {
    var STATE_ID = $(this).val();
	fetchstate();
	var BANK_ID=$("select[name='BANK']").val();
   if(STATE_ID) {
        $.ajax({
            url: "ajax2.php",
            dataType: 'Json',
            data: {'id':STATE_ID,'id2':BANK_ID},
            success: function(data) {
                $('select[name="DISTRICT"]').empty();
				$('select[name="DISTRICT"]').append('<option value="">--select district--</option>');
                $.each(data, function(key, value) {
                    $('select[name="DISTRICT"]').append('<option value="'+ key +'">'+ value +'</option>');
                });
            }
        });
    }else{
        $('select[name="DISTRICT"]').empty();
    }
});


$( "select[name='DISTRICT']" ).change(function () {
	var DIST_ID = $(this).val();
	fetchdist();
    var STATE_ID=$("select[name='STATE']").val();
	var BANK_ID=$("select[name='BANK']").val();
    if(DIST_ID) {
        $.ajax({
            url: "ajax3.php",
            dataType: 'Json',
            data: {'id':STATE_ID,'id1':DIST_ID,'id2':BANK_ID},
            success: function(data) {
                $('select[name="CITY"]').empty();
				$('select[name="CITY"]').append('<option value="">--select city--</option>');
                $.each(data, function(key, value) {
                    $('select[name="CITY"]').append('<option value="'+ key +'">'+ value +'</option>');
                });
            }
        });
    }else{
        $('select[name="CITY"]').empty();
    }
});

$( "select[name='CITY']" ).change(function () {
	var CITY_ID = $(this).val();
	fetchcity();
    var STATE_ID=$("select[name='STATE']").val();
    var DIST_ID=$("select[name='DISTRICT']").val();
	var BANK_ID=$("select[name='BANK']").val();
    if(CITY_ID) {
        $.ajax({
            url: "ajax4.php",
            dataType: 'Json',
            data: {'id':STATE_ID,'id1':DIST_ID,'id2':CITY_ID,'id3':BANK_ID},
            success: function(data) {
                $('select[name="BRANCH"]').empty();
				$('select[name="BRANCH"]').append('<option value="">--select branch--</option>');
                $.each(data, function(key, value) {
                    $('select[name="BRANCH"]').append('<option value="'+ key +'">'+ value +'</option>');
                });
            }
        });
    }else{
        $('select[name="BRANCH"]').empty();
    }
});
</script>
<script>
$(document).ready(function(){
  $("#searchele").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("table tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
  $('select').select2();
});
</script>
</body>