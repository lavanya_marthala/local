<!DOCTYPE html>
<html>
<head>
<style>
table {
    width: 100%;
    border-collapse: collapse;
}

table, td, th {
    border: 1px solid black;
    padding: 5px;
}

th {text-align: left;}
</style>
</head>
<body>

<?php
$search=$_GET["search"];

$con = mysqli_connect('localhost','root','','ifsccodes');
if (!$con) {
    die('Could not connect: ' . mysqli_error($con));
}

$sql="select bankmaster.IFSC,bankmaster.MICRCODE,bankmaster.ADDRESS,bankmaster.CONTACT,bank.BANK,branch.BRANCH_NAME,cities.CITY,districts.DISTRICT,states.STATE from bankmaster
inner join bank on bank.BANK_ID=bankmaster.BANK_ID
inner join branch on branch.BRANCH_ID=bankmaster.BRANCH_ID
inner join cities on cities.CITY_ID=bankmaster.CITY_ID
inner join districts on districts.DISTRICT_ID=bankmaster.DISTRICT_ID
inner join states on states.STATE_ID=bankmaster.STATE_ID
where bank.bank LIKE '%".$search."%' or branch.BRANCH_NAME LIKE '%".$search."%' or cities.CITY LIKE '%".$search."%' or districts.DISTRICT LIKE '%".$search."%' or states.STATE LIKE '".$search."' or bankmaster.IFSC LIKE '%".$search."%' or bankmaster.MICRCODE LIKE '%".$search."%'" ;
$result = mysqli_query($con,$sql);

echo "<table style='width:700px;'>
<tr>
<th>bank</th>
<th>IFSC</th>
<th>MICRCODE</th>
<th>CONTACT</th>
<th>ADDRESS</th>
<th>city</th>
<th>district</th>
<th>state</th>
<th>branch</th>
</tr>";
while($row = mysqli_fetch_array($result)) {
    echo "<tr>";
    echo "<td><a href='profile.php?id=".$row['BANK']."' >" . $row['BANK'] . "</a></td>";
	echo "<td><a href='profile.php?id=".$row['IFSC']."'>" . $row['IFSC'] . "</a></td>";
	echo "<td><a href='profile.php?id=".$row['MICRCODE']."'>" . $row['MICRCODE'] . "</a></td>";
	echo "<td>" . $row['CONTACT'] . "</td>";
	echo "<td>" . $row['ADDRESS'] . "</td>";
    echo "<td>" . $row['CITY'] . "</td>";
    echo "<td>" . $row['DISTRICT'] . "</td>";
    echo "<td><a href='profile.php?id=".$row['STATE']."'>" . $row['STATE'] . "</a></td>";
	echo "<td><a href='profile.php?id=".$row['BRANCH_NAME']."'>" . $row['BRANCH_NAME'] . "</a></td>";
    echo "</tr>";
}
echo "</table>";
mysqli_close($con);
?>
</body>
</html>