<?php
//index.php
include("dbconnection.php");
//$connect = mysqli_connect("dallas142", "jtrust08_auction", "abc1234", "jtrust08_auction");
$query = "SELECT * FROM institution_master where institutiondelete=1";
$result = mysqli_query($connect, $query);
?>
<!DOCTYPE html>
<html>
 <head>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

 </head>
 <body>
   <input class="form-control" id="myInput" type="text" placeholder="Search..">
  <br /><br />
  <div class="container">

   <br />
   <div class="table-responsive">
    <table class="table table-striped table-bordered">
      <tbody id="myTable">
        <thead>
     <tr>
      <th>Bank Id</th>
      <th>Bank Name</th>
      <th>Bank city</th>
      <th>View</th>
     </tr>
   </thead>
   <tbody id="myTable">
     <?php
     while($row = mysqli_fetch_array($result))
     {
      echo '
      <tr>
       <td>'.$row["institutionid"].'</td>
       <td>'.$row["institutionname"].'</td>
       <td>'.$row["institutioncity"].'</td>
       <td><a href="institutionprofile.php?institutionid='.$row["institutionid"].'">View</a></button></td>
      </td>
      </tr>
      ';
     }
     ?>
     </tbody>
    </table>
   </div>

  </div>
 </body>
</html>

<script>
$(document).ready(function(){
  $("#myInput").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#myTable tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
</script>
