<?php include("header.php");
$bid=$_GET["loginid"];

?>
<div class="container">
  <h2>Dynamic Tabs</h2>
  <p>To make the tabs toggleable, add the data-toggle="tab" attribute to each link. Then add a .tab-pane class with a unique ID for every tab and wrap them inside a div element with class .tab-content.</p>

  <ul class="nav nav-tabs">
    <li class="active"><a data-toggle="tab" href="bankhome">Home</a></li>
    <li><a data-toggle="tab" href="#bankformat">Upload format</a></li>
    <li><a data-toggle="tab" href="#bankuploaddata">Upload data</a></li>
    <li><a data-toggle="tab" href="#bankviewupload">View uploaded data</a></li>
  </ul>

  <div class="tab-content">
    <div id="bankhome" class="tab-pane fade in active">
      <!--<?php include 'bank_upload/bankhome.php';?>-->


    </div>
    <div id="bankformat" class="tab-pane fade">

      <?php include 'bank_upload/bankformat.php';?>


      </div>
    <div id="bankuploaddata" class="tab-pane fade">
      <?php include 'bank_upload/index.php';?>
        </div>
    <div id="bankviewupload" class="tab-pane fade">
      <?php include 'bank_upload/bankviewupload.php';?>
    </div>
  </div>
</div>

</body>
</html>
<?php include("footer.php") ?>
