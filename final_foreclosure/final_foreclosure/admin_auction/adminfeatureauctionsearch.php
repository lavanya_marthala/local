<?php
//index.php
include("dbconnection.php");
//$connect = mysqli_connect("dallas142", "jtrust08_auction", "abc1234", "jtrust08_auction");
$query = "SELECT * FROM auction_master  where auctionapproval='1' and auctiondelete=1 ORDER BY auctionid ASC";
$result = mysqli_query($connect, $query);
?>
<!DOCTYPE html>
<html>
 <head>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

 </head>
 <body>
   <input class="form-control" id="myinputauction" type="text" placeholder="Search..">
  <br /><br />
  <div class="containerauction">

   <br />
   <div class="table-responsive">
    <table class="table table-striped table-bordered">
      <tbody id="mytableauction">
        <thead>
     <tr>
      <th>Auction Id</th>
      <th>Bank Name</th>
      <th>Auction city</th>
      <th>Auction price</th>
      <th>View</th>
     </tr>
   </thead>
   <tbody id="mytableauction">
     <?php
     while($row = mysqli_fetch_array($result))
     {
      echo '
      <tr>
       <td>'.$row["auctionid"].'</td>
       <td>'.$row["bankid"].'</td>
       <td>'.$row["auctioncity"].'</td>
       <td>'.$row["auctionprice"].'</td>
       <td><a href="auctionprofile.php?auctionid='.$row["auctionid"].'">View</a></button></td>
      </tr>
      ';
     }
     ?>
     </tbody>
    </table>
   </div>

  </div>
 </body>
</html>
<script>
$(document).ready(function(){
  $("#myinputauction").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#mytableauction tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
</script>
