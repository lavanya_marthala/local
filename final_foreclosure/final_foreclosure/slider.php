<!DOCTYPE html>
<html>
<head>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script type="text/javascript">

  $(function() {
    $( "#slider-range" ).slider({
      range: true,
      min: 0,
      max: 50000,
      values: [ 100, 300 ],
      slide: function( event, ui ) {
        console.log(ui.values);
        $( "#amount" ).val( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] );
		$( "#amount1" ).val(ui.values[ 0 ]);
		$( "#amount2" ).val(ui.values[ 1 ]);
      }
    });
    $( "#amount" ).html( "$" + $( "#slider-range" ).slider( "values", 0 ) +
     " - $" + $( "#slider-range" ).slider( "values", 1 ) );
  });

  </script>
</head>

<body>

  <p>
    Price Range:<p id="amount"></p>
  </p>

  <div id="slider-range"></div>

  <form method="post" action="user_features/getauctions.php">
    <input type="text" name="amount1" id="amount1">
    <input type="text" id="amount2" name="amount2">
    <input type="submit" name="submit_range" value="Submit">
  </form>

</body>
</html>
<script>
$(document).ready(function(){
     $('#min_price').change(function(){
          var minprice = $(this).val();
          $("#price_range").text("Product under Price Rs." + price);
          $.ajax({
               url:"load_product.php",
               method:"POST",
               data:{price:price},
               success:function(data){
                    $("#product_loading").fadeIn(500).html(data);
               }
          });
     });
});
</script>
