<?php include("header.php") ?>
<div class="container">

<br><br>
  <ul class="nav nav-tabs">
    <li class="active"><a data-toggle="tab" href="#home">Home</a></li>
    <li><a data-toggle="tab" href="#adminbanks">Banks</a></li>
    <li><a data-toggle="tab" href="#adminauctions">Auctions</a></li>
    <li><a data-toggle="tab" href="#adminusers">Users</a></li>
  </ul>

  <div class="tab-content">
    <div id="home" class="tab-pane fade ">
      
      <?php include 'adminfeaturehome.php';?>
      </div>
    <div id="adminbanks" class="tab-pane fade">
      <?php include 'admin_bank/adminfeaturebank.php';?>
      </div>

    <div id="adminusers" class="tab-pane fade">
        <?php include 'admin_user/adminfeatureuser.php';?>
      </div>
    <div id="adminauctions" class="tab-pane fade">
      <?php include 'admin_auction/adminfeatureauction.php';?>
    </div>
  </div>
</div>

</body>
</html>
<?php include("footer.php") ?>
